from django.test import TestCase

# Create your tests here.
class story8Test(TestCase):
    def test_url_exist(self):
        response = self.client.get('/story8/')
        self.assertEqual(response.status_code,200)

    def test_templates(self):
        response = self.client.get('/story8/')
        self.assertTemplateUsed(response,'story8.html')

    

