from django.test import TestCase,Client

# Create your tests here.
class Story7Test(TestCase):
    def test_url_exist(self):
        response = self.client.get('/story7/')
        self.assertEqual(response.status_code,200)
    
    def test_templates(self):
        response = self.client.get('/story7/')
        self.assertTemplateUsed(response,'profile.html')