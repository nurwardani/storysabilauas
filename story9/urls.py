from django.urls import path,include
from . import views

urlpatterns = [
    path('logout/', views.LogOutView, name='logout'),
    path('hasil/',views.Index, name='hasil'),
    path('login/',views.LoginView, name='login'),
]