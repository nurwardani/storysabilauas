from django.shortcuts import render, redirect
from django.contrib.auth import authenticate,login, logout

# Create your views here.

def Index(request):
    context={
        'page_title':'Hola',
    }

    print(request.user.is_authenticated)

    template_name = None
    #melakukan pengecekkan ketika ke login 
    if request.user.is_authenticated:
        template_name = 'hasilUser.html'
    else:
        template_name = 'hasil.html'

    return render(request,template_name,context)

def LoginView(request):
    context= {
        'page_title' : 'login',
    }
    if request.method == "POST":
        username_login = request.POST['username']
        password_login = request.POST['password']

        #Melakukan authenticate
        user = authenticate(request, username=username_login, password = password_login)
    
        #cek si user itu ada gak
        if user is not None :
            login(request, user)
            #Melakukan redirect ke url path name='hasil'
            return redirect('hasil')    
        else:
            return redirect('login')

        
        

    return render(request,'login.html', context)

def LogOutView(request):
    context = {
        'page_title':'logout',
    }

    if request.method == "POST":
        if request.POST["logout"] == "Submit":
            logout(request)

        return redirect('login')

    return render(request, 'logout.html', context)
