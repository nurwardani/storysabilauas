from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import TulisKabar
from .models import Status
from .forms import StatusForm
from django.utils import timezone
from django.http import HttpRequest
from datetime import date
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


# Create your tests here.

class LandingPageTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code,200)
    
    def test_templates(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response,'landing_page.html')
    
    def test_model_create_new_status(self):
        status_baru = Status.objects.create(date=timezone.now(), kabar= 'sekarang lagi galau')
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object,1)

    def test_save_POST_request(self):
        response = self.client.post('', data ={'date' : '2019-10-31T13:30', 'kabar' : 'bucinan'} )
        counting_status_object = Status.objects.all().count()
        self.assertEqual(counting_status_object,1)
        self.assertEqual(response.status_code,200)
        new_response = self.client.get('')
        html_response = new_response.content.decode('utf8')
        self.assertIn('bucinan',html_response)
    
    def test_forms_is_valid(self):
        form_data = {'kabar':'sesuatu bangetttttttttttttttzzzzzzzzz'}
        form = StatusForm(data=form_data)
        self.assertTrue(form.is_valid())
    
    def test_forms_is_not_valid(self):
        form_data = {'status': 'Membuat website baru yang membuat saya sedikit zbl. Karena kita diminta membuat herokuapp baru dan berarti repo baru. Saya berharap lab dan Story PPW bisa berjalan dalam satu repository saja hingga akhir, kalaupun dibutuhkan maksimal dua repository dan herokuapp. Tapi nyatanya sudah lebih dari 3 repo dan herokuapp baru yang dibutuhkan hingga sekarang. Tapi terlepas dari hal itu, membuat website dengan TDD membuka pandangan baru tentang website development. Pada awalnya pengerjaan TDD terlihat sangat meribetkan karena tentunya kita cenderung lebih suka mengetest website secara langsung di localhost. Tapi hal ini sebenarnya tidak efisien dan tidak bisa mengcover segala kesalahan yang mungkin akan terjadi di belakang website. Dalam membuat website dengan TTD tentu langkah pertama yang dilakukan adalah membuat project baru dan app baru, lalu melakukan configurasi setting seperti di story dan lab sebelumnya. Dan sebelum melakukan yang lainnya, kita bisa mulai membuat Unit Test di app. Dalam struktur Django, dalam setiap app akan tersedia test.py untuk melakukan TDD. Kita bisa mulai mengedit test dengan men-deklarasikan class untuk Unit Test. Dalam setiap Unit test kita bisa membuat sebuah fungsi untuk testing kerja website mulai dari response client hingga tulisan bisa kita test. Dalam satu file test kita bisa membuat banyak Unit Test dan fungsi test. Membuat Unit Test adalah salah satu bentuk dari TDD. Setelah semua test dibuat, saya membuat README.md berisi badge dan keterangan lebih lanjut. Badge dibuat untuk melihat coverage test terhadap program kita. Badge terdiri dari badge pipeline dan coverage. Untuk memproses coverage kita bisa memasukkan “TOTAL\s+\d+\s+\d+\s+(\d+)%” di general pipeline CI/CD gitlab. Lalu deploy ke gitlab.'}
        form = StatusForm(data=form_data)
        self.assertFalse(form.is_valid())

class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_input_status(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/'))
        kabar = self.selenium.find_element_by_name('kabar')
        submit = self.selenium.find_element_by_id('id_kirim')
        kabar.send_keys('Bucins Oppa')
        time.sleep(5)
        submit.send_keys(Keys.RETURN)
        time.sleep(5)

