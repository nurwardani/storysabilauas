from django.urls import path
from . import views

urlpatterns = [ 
    path("", views.TulisKabar, name='landing_page'),
]
