from django import forms
from .models import Status

class StatusForm(forms.ModelForm):
    class Meta:
        model   = Status
        fields  = [
            'kabar', 
        ]
        widgets ={
            'kabar' : forms.TextInput(
                attrs = {
                    'class'         : 'form-control',
                    'placeholder'   : 'tulis kabarmu',
                }
            )
        }