from django.shortcuts import render

# Create your views here.

from .forms import StatusForm
from .models import Status


def TulisKabar(request):
    posts = Status.objects.all()    
    post_form = StatusForm(request.POST or None)

    if request.method == 'POST':
        # cover : no pragma
        if post_form.is_valid():
            # cover : no pragma
            post_form.save()    
    
    context = {
        'post_form' : post_form,
        'posts' : posts,
    }

    return render(request,'landing_page.html',context)
    

