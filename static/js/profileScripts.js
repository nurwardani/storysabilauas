$(document).ready(function() {

    $("#actButton").click(function() {
        $("#panel1").slideToggle();
        $("#panel2").hide();
        $("#panel3").hide();
    });

    $("#eduButton").click(function() {
        $("#panel2").slideToggle();
        $("#panel1").hide();
        $("#panel3").hide();
    });

    $("#achievButton").click(function() {
        $("#panel3").slideToggle();
        $("#panel2").hide();
        $("#panel1").hide();
    });

    var clicked = false;
    $("#switch").on('click', function() {
        if (clicked) {
            $("body").css("background-color", "#FFFFFF");
            $(".dekskripsi").css("color", "black");
            $(".accordion ").css("border-color", "#FFFFFF");
            clicked = false;
        } else {
            $("body").css("background-color", "#000000");
            $(".dekskripsi").css("color", "white");
            $(".accordion").css("border-color", "#000000");
            clicked = true;
        };
        return false;
    });

});