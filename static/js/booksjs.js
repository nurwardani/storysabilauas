$(document).ready(function() {
    $("#tekanCari").click(function() {
        var query = $("#search").val();
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + query,
            datatype: "json",
            success: function(result) {
                if (result.totalItems == 0) {
                    $('#tabel').css({ 'display': 'none' });
                    $('p').css({ 'display': 'inline-block' });
                    $('p').text();

                }
                $('#table-books').empty();

                $.each(result.items, function(i, item) {
                    $('#tabel').css({ 'display': 'block' });
                    $('p').css({ 'display': 'none' });
                    $('<tr>').append(
                        $('<td>').append('<img style="max-height: 150px;max-width: 150px" src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
                        $('<td>').text(item.volumeInfo.title),
                        $('<td>').text(item.volumeInfo.authors),
                        $('<td>').text(item.volumeInfo.publisher),
                        $('<td>').append('<a href="' + item.volumeInfo.canonicalVolumeLink + '" target="_blank">LINK</a>')
                    ).appendTo("#table-books");
                });

            },

            type: "GET",
        });

    });
    $("#search").val("edu");
    $("#tekanCari").click();
});